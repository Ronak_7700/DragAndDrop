$.fn.drag = function(e){
	console.log(e);
	let moused = false;
	let ele = $(this);
	let handle = $(this);
	let dropZone = [];
	let startPos = this.offset();
	startPos.position = "inherit";
	ele.attr("draggable","false")
	let xy = [];
	if(typeof e != "undefined"){
		if('handle' in e){
			if(ele.find(e.handle).length > 0)
			handle = ele.find(e.handle);
		}
		if('dropZone' in e){
			$(e.dropZone).each(function(e,b)
					{
						dropZone.push(b);
					}
		)
			dropZone.push($(e.dropZone));
		}
	}
	handle.on("mousedown",function(event) {
		xy =[event.clientX - ele.offset().left ,event.clientY - ele.offset().top];
		moused = true;
		ele.addClass('pickstart')
	});

	$(document).on("mousemove",function(e) {
		if(!moused)return;
		//console.log(event.pageX,event.pageY);
		//console.log(e
		e.preventDefault();
		ele.addClass('pickup')
		ele.css(
			{
				position:"fixed",
				left:e.pageX-xy[0],
				top:e.pageY-xy[1]
			}
		);
	});
	$(document).on("mouseup",function(e){
		if(!moused)return;
		moused = false;
		console.log(e);
		ele.removeClass('pickup')
		var checkDrop = null;
		document.elementsFromPoint(e.clientX,e.clientY).forEach(function(e){
			if(dropZone.includes(e)){
				checkDrop = e;

			}
		});
		if(checkDrop != null){
			ele.appendTo(checkDrop)
			startPos = $(checkDrop).offset();
				startPos.position = "inherit";
			ele.css($(checkDrop).offset());
			console.log("droped");
		}else{
			if(dropZone.length != 0){
				ele.css(startPos);
			}
			console.log("not droped");
		}
	});
};
